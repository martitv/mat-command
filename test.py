#!/usr/bin/env python

import urllib.request, urllib.error, urllib.parse, sys
from datetime import date
from bs4 import BeautifulSoup
from html.parser import HTMLParser

#Fredrikke cafe url
urlFred = 'http://www.sio.no/wps/portal/!ut/p/c5/04_SB8K8xLLM9MSSzPy8xBz9CP0os3gDfwNvJ0dTP0NXAyNDA38TC3cDKADKR2LKmyDkidGNAzgS0h0Oci1-28HyuM3388jPTdUvyA2NMMgyUQQAAcWpkQ!!/dl3/d3/L0lDU0lKSWdrbUEhIS9JRFJBQUlpQ2dBek15cXchLzRCRWo4bzBGbEdpdC1iWHBBRUEhLzdfME8wS0JBNU4xRTBNSDJWMzVQMDAwMDAwMDAvN2x0YlQ2Mzk3MDAxOQ!!/?WCM_PORTLET=PC_7_0O0KBA5N1E0MH2V35P00000000000000_WCM&WCM_GLOBAL_CONTEXT=/wps/wcm/connect/migration/sio/mat+og+drikke/dagens+middag/frederikke+spiseri'
#IFI cafe url
urlIFI = 'http://www.sio.no/wps/portal/!ut/p/c5/04_SB8K8xLLM9MSSzPy8xBz9CP0os3gDfwNvJ0dTP0NXAyNDA38TC3cDKADKR2LKmyDkidGNAzgS0h0Oci1-28HyuM3388jPTdUvyA2NMMgyUQQAAcWpkQ!!/dl3/d3/L0lDU0lKSWdrbUEhIS9JRFJBQUlpQ2dBek15cXchLzRCRWo4bzBGbEdpdC1iWHBBRUEhLzdfME8wS0JBNU4xRTBNSDJWMzVQMDAwMDAwMDAvN2x0YlQ2Mzk3MDAxOQ!!/?WCM_PORTLET=PC_7_0O0KBA5N1E0MH2V35P00000000000000_WCM&WCM_GLOBAL_CONTEXT=/wps/wcm/connect/migration/sio/mat+og+drikke/dagens+middag/informatikkafeen'

class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.fed = []
        self.addFlag = False
    def handle_data(self, d):
        if self.addFlag:
            self.fed[-1] += d
            self.addFlag = False
        else:
            self.fed.append(d)
    def handle_entityref(self, ref):
        self.fed[-1] += self.unescape("&%s;" % ref)
        self.addFlag = True
    def get_data(self):
        return self.fed
		
enToNo = {'Monday':'Mandag', 'Tuesday':'Tirsdag', 'Wednesday':'Onsdag', 'Thursay':'Torsdag', 'Friday':'Fredag'}

#Strips tags from given html
def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

#Format function for fredrikke
#Returns  a formatted string
def format_fred(data):
	tmp = [x for x in data if x not in ['\n']]
	dict = {}
	iD = tmp.index("Dagens:")
	iV = tmp.index("Vegetar: ")
	iH = tmp.index("Halal:")
	
	dict[tmp[:iV][0][:-1].lower()] = tmp[:iV][1:]
	dict[tmp[iV:][0][:-2].lower()] = tmp[iV:iH][1:]
	dict[tmp[iH:][0][:-1].lower()] = tmp[iH:][1:]
	for i in dict[sys.argv[2].lower()]:
		print(i)
	print("----------------")
	return '\n'.join(dict[sys.argv[2].lower()])

#Format function for fredrikke
#Returns a formatted string.
def format_ifi(data):
	#.encode('utf8', 'ignore') if characters bugs
	data = [x for x in data if x not in ['\n', '\xc2\xa0', '\xa0', 'Dagens: ', 'Vegetar:']]
	days = data[:5]
	food = data[5:]
	food.append("No veggie today D:")

	result = {}
	day = date.today().strftime("%A")
	#resultStr = ""
	for i in range(len(days)):
		#resultStr += days[i] + ":\n" + "\tDagens : " + food[i*2] + "\n\tVegetar: " + food[i*2+1] + "\n\n"
		result[days[i]] = (food[i*2], food[i*2+1])
	print(result)
	return '{0}:\n \tDagens : {1}\n\tVegetar: {2}'.format(enToNo[day], result[enToNo[day]][0], result[enToNo[day]][1])
		


url = urlFred
format_func = format_fred
if len(sys.argv) < 2:
	print("argument ERROR")
	sys.exit(0)
elif sys.argv[1].lower() == "ifi":
	url = urlIFI
	format_func = format_ifi
elif sys.argv[1].lower() == "fred":
	url = urlFred
	format_func = format_fred
	
#Opens given url and returns html
page = urllib.request.urlopen(url, timeout=10)
#Html parsing
soup = BeautifulSoup(page)

#Finds the data we are looking for and feeds that to the formatting function.
divtag = soup.find_all('div', {'class': 'sioArticleBodyText'})
if len(divtag) > 0:
	tabletag = divtag[0].find_all('table')
	trtag = tabletag[0].find_all('tr')	
	text = str.join('',list(map(str,trtag)))
	data = strip_tags(text)
	print(format_func(data))
else:
	print("ERROR")
page.close()
